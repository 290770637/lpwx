# 说明
 1.src/main/resources/wx.setting文件中写入对应配置属性<br />
 2.在有公网独立IP的环境下启动项目<br />
 3.在微信公众号管理中填写http://localhost/wx并校验服务器，配置文件中token要与页面一致<br />
 4.关注公众号，发送测试，即可获取appKey

## 调用

调用地址http://localhost/send ，参数为
appKey=appKey,title=标题,content=内容<br />
例：http://localhost/send?appKey=IOekeD1d_wYL1OWOx5cZAycU&title=系统消息&content=服务器出错了，请尽快修复