package cn.loyle.pojo;


import com.xiaoleilu.hutool.util.XmlUtil;
import org.osgl.http.H;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.InputStream;

/**
 * 接收微信消息类
 */
public class WxMessage {
    private String ToUserName;
    private String FromUserName;
    private String CreateTime;
    private String MsgType;
    private String Content;
    private String MsgId;

    public WxMessage(H.Request req) {
        Document document = XmlUtil.readXML(req.inputStream());
        Element rootElement = XmlUtil.getRootElement(document);
        this.ToUserName = XmlUtil.elementText(rootElement, "ToUserName");
        this.FromUserName = XmlUtil.elementText(rootElement, "FromUserName");
        this.CreateTime = XmlUtil.elementText(rootElement, "CreateTime");
        this.MsgType = XmlUtil.elementText(rootElement, "MsgType");
        this.Content = XmlUtil.elementText(rootElement, "Content");
        this.MsgId = XmlUtil.elementText(rootElement, "MsgId");
    }

    public WxMessage() {

    }

    public String getToUserName() {
        return ToUserName;
    }

    public void setToUserName(String toUserName) {
        ToUserName = toUserName;
    }

    public String getFromUserName() {
        return FromUserName;
    }

    public void setFromUserName(String fromUserName) {
        FromUserName = fromUserName;
    }

    public String getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(String createTime) {
        CreateTime = createTime;
    }

    public String getMsgType() {
        return MsgType;
    }

    public void setMsgType(String msgType) {
        MsgType = msgType;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getMsgId() {
        return MsgId;
    }

    public void setMsgId(String msgId) {
        MsgId = msgId;
    }
}
