package cn.loyle.pojo;

import com.thoughtworks.xstream.XStream;
import com.xiaoleilu.hutool.date.DateUtil;

/**
 * 返回微信消息类
 */
public class TextMsg {
    private String ToUserName;
    private String FromUserName;
    private long CreateTime;
    private String MsgType;

    public TextMsg(WxMessage message, String content) {
        super();
        ToUserName = message.getFromUserName();
        FromUserName = message.getToUserName();
        CreateTime = DateUtil.current(false);
        MsgType = "text";
        Content = content;
    }

    @Override
    public String toString() {
        return "TextMsg [ToUserName=" + ToUserName + ", FromUserName="
                + FromUserName + ", CreateTime=" + CreateTime + ", MsgType="
                + MsgType + ", Content=" + Content + "]";
    }

    private String Content;

    public TextMsg(String toUserName, String fromUserName, long createTime,
                   String msgType, String content) {
        super();
        ToUserName = toUserName;
        FromUserName = fromUserName;
        CreateTime = createTime;
        MsgType = msgType;
        Content = content;
    }

    public TextMsg() {
        super();
    }

    public String getToUserName() {
        return ToUserName;
    }

    public void setToUserName(String toUserName) {
        ToUserName = toUserName;
    }

    public String getFromUserName() {
        return FromUserName;
    }

    public void setFromUserName(String fromUserName) {
        FromUserName = fromUserName;
    }

    public long getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(long createTime) {
        CreateTime = createTime;
    }

    public String getMsgType() {
        return MsgType;
    }

    public void setMsgType(String msgType) {
        MsgType = msgType;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String toMessage() {
        XStream xStream = new XStream();
        xStream.alias("xml", this.getClass());
        String textMsg2Xml = xStream.toXML(this);
        return textMsg2Xml;
    }
}