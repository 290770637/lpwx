package cn.loyle;

import act.Act;

/**
 * Hello world!
 *
 */
public class App {
    /**
     * 入口
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        Act.start("LPWX");
    }
}
