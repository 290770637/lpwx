package cn.loyle.service;

import act.app.ActionContext;
import act.controller.Controller;
import act.event.EventBus;
import org.osgl.http.H;
import org.osgl.web.util.UserAgent;

import javax.inject.Inject;

public class Global extends Controller.Util {

    @Inject
    protected ActionContext context;

    @Inject
    protected H.Session session;

    @Inject
    protected UserAgent ua;

    @Inject
    protected H.Flash flash;

    @Inject
    protected EventBus eventBus;
}
