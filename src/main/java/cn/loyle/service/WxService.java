package cn.loyle.service;

import cn.loyle.pojo.TextMsg;
import cn.loyle.pojo.WxMessage;
import cn.loyle.utils.WxUtil;
import com.xiaoleilu.hutool.date.DateUtil;
import org.osgl.mvc.annotation.Action;
import org.osgl.mvc.annotation.GetAction;
import org.osgl.mvc.annotation.PostAction;

public class WxService extends Global{
    /**
     * 校验微信token或校验服务器是否在线
     * @return
     */
    @GetAction("wx")
    public String main(){
        if (WxUtil.checkIsWx(context,WxUtil.setting.getStr("token"))){
            return context.req().paramVal("echostr");
        }else {
            return DateUtil.now();
        }
    }

    /**
     * 接收处理微信消息
     * @return
     */
    @PostAction("wx")
    public String onMessage(){
        WxMessage message = new WxMessage(context.req());
        if ("1".equals(message.getContent())){
           return new TextMsg(message,"1是啥？").toMessage();
        }else if ("注册".equals(message.getContent())){
            //发送“注册”获取用户ID，可用于发送模板消息
            return new TextMsg(message,message.getFromUserName()).toMessage();
        }else {
            return new TextMsg(message,"有事吗？").toMessage();
        }
    }

    /**
     * 对外发布的自动发送模板消息接口
     * 此功能参考自http://sc.ftqq.com/3.version
     * @param appKey 微信用户ID，可通过关注公众号发送“注册”获取
     * @param title
     * @param content
     * @return
     */
    @Action("send")
    public String send(String appKey,String title,String content){
        return WxUtil.sendWithTemplet(appKey, title, content);
    }
}
